<?php
    class upload{
        public $src = './uploads/';
        public $tmp;
        public $filename;
        public $type;
        public $uploadfile;
        
        function __construct(){
            $this -> filename = $_FILES['fileToUpload']['name'];
            $this -> tmp = $_FILES['fileToUpload']['tmp_name'];
            $this -> uploadfile = $this -> src.basename($this -> filename);
            $this -> size = $_FILES['fileToUpload']['size'];
            $this -> filetype = pathinfo($this -> uploadfile, PATHINFO_EXTENSION);
        }
        
        public function uploadfile(){
            
           // if(!getimagesize($this -> tmp)){
//                echo "File is not an image <br>";
//                return false;
//            }
            echo  $this -> filetype;
            if($this -> filetype != 'jpg' && $this -> filetype != 'jpeg' && $this -> filetype != 'png' && $this -> filetype != 'gif'){
                echo "Sorry file is not an image";
                return false;
            }
            
            if($this -> size > 5000000){
                echo "sorry your image is too large";
                return false;
            }
            
            if(move_uploaded_file($this -> tmp, $this -> uploadfile)){
                return true;
            }
        }
    }

?>