<?php
    
    include('class.crud.php');
    include('upload.php');
    
    $crud = new crud();
    
    
    $product_id = ISSET($_REQUEST['product_id']) ? $_REQUEST['product_id'] : '';
    $flag = $_REQUEST['flag'];
    
    if(ISSET($_POST['submit'])){
        
    $product_name = ISSET($_POST['product_name']) ? $_POST['product_name'] : '';
    $category_id = ISSET($_POST['category']) ? $_POST['category'] : '';
        
        if($flag == 'i'){
           
            if(ISSET($_FILES['fileToUpload'])){
                $upload = new upload();
               
                if($upload -> uploadfile()){
                    echo 'File has been uploaded';
                     $crud->insert($category_id, $product_name, $upload -> filename);
                     header("Location: index.php");
                } else {
                    echo 'File could not be uploaded';
                }
            }
           
        } else if($flag == 'u'){
            $crud->update($category_id, $product_name, $product_id);
             header("Location: index.php");
        }  
   
      } else {
            if($flag == 'd'){
                $crud->delete($product_id);
                 header("Location: index.php");
            }
      }
                
        
        mysqli_close($conn);
    
?>