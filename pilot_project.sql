-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2016 at 08:57 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pilot_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `create_ts`, `update_ts`) VALUES
(1, 'Household', '2016-07-30 07:47:34', '0000-00-00 00:00:00'),
(2, 'Vehicle', '2016-07-30 07:47:34', '0000-00-00 00:00:00'),
(3, 'Computer_accesory', '2016-07-30 07:47:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `category_id`, `product_name`, `image_name`, `create_ts`, `update_ts`) VALUES
(23, 1, 'pencil', '', '2016-08-04 12:37:02', '0000-00-00 00:00:00'),
(24, 2, 'car', '', '2016-08-05 05:15:31', '0000-00-00 00:00:00'),
(25, 3, 'mouse', '', '2016-08-05 05:15:39', '0000-00-00 00:00:00'),
(26, 1, 'car', '', '2016-08-05 06:08:06', '0000-00-00 00:00:00'),
(27, 1, 'car', '', '2016-08-05 06:08:58', '0000-00-00 00:00:00'),
(28, 1, 'car', '', '2016-08-05 06:11:08', '0000-00-00 00:00:00'),
(29, 1, 'car', '', '2016-08-05 06:12:22', '0000-00-00 00:00:00'),
(30, 1, 'car', '', '2016-08-05 06:13:51', '0000-00-00 00:00:00'),
(31, 1, 'car', '', '2016-08-05 06:15:47', '0000-00-00 00:00:00'),
(32, 1, 'car', '', '2016-08-05 06:16:25', '0000-00-00 00:00:00'),
(33, 1, 'car', '', '2016-08-05 06:17:22', '0000-00-00 00:00:00'),
(34, 1, 'car', 'Desert.jpg', '2016-08-05 06:29:36', '0000-00-00 00:00:00'),
(35, 1, 'bus', 'Hydrangeas.jpg', '2016-08-05 07:11:31', '0000-00-00 00:00:00'),
(36, 1, 'bus', 'Koala.jpg', '2016-08-05 07:14:38', '0000-00-00 00:00:00'),
(37, 1, 'bus', 'Desert.jpg', '2016-08-05 07:21:47', '0000-00-00 00:00:00'),
(38, 2, 'car', 'Chrysanthemum.jpg', '2016-09-19 06:56:02', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
